package br.ucsal.bes20202.testequalidade.restaurante.business;

public class RestauranteBO2Test {

	/**
	 * Método a ser testado:
	 * 
	 * public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException.
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa livre apresenta sucesso,
	 * retornando o número da comanda.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. NÃO é necesário fazer o mock para o getSituacao;
	 * 
	 * 3. Você DEVE mocar a instânciação da Comanda (mock de construtor);
	 * 
	 * 4. Além de verificar o número da comanda, você deverá usar o verify para se
	 * certificar de que a comanda foi incluída pelo ComandaDAO, ou seja, o
	 * ComandaDAO.incluir(comanda) foi chamado. É necessário garantir que a comanda
	 * incluída é para a mesa informada.
	 * 
	 */
	public void testarAbrirComandaMesaLivre() {
	}
}
