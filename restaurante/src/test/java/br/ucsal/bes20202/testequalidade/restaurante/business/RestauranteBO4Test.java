package br.ucsal.bes20202.testequalidade.restaurante.business;

public class RestauranteBO4Test {

	/**
	 * Método a ser testado:
	 * 
	 * public Double fecharComanda(Integer codigoComanda) throws
	 * RegistroNaoEncontrado, ComandaFechadaException
	 * 
	 * Verificar se o fechamento de uma comanda ABERTA retorna o valor total para
	 * mesma é retornado e a situaçao da mesma muda para FECHADA.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar as classes Item e Comanda;
	 * 
	 * 2. Você deve fazer o mock apenas para a classe ComandaDao, os demais métodos
	 * devem ter execução normal (os métodos getValorUnitario, getItens e
	 * setSituacao têm comportamento básico, não sendo necessário fazer mock para os
	 * mesmos);
	 * 
	 */
	public void testarFecharComandaAberta() {
	}
}
