package br.ucsal.bes20202.testequalidade.restaurante.business;

public class RestauranteBO1Test {

	/**
	 * Método a ser testado:
	 * 
	 * public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException.
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa ocupada levanta a
	 * exceção MesaOcupadaException.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. NÃO é necesário fazer o mock para o getSituacao;
	 * 
	 */
	public void testarAbrirComandaMesaOcupada() {
	}
}
