package br.ucsal._20211.testequalidade.restaurante.domain;

import java.util.HashMap;
import java.util.Map;

import br.ucsal._20211.testequalidade.restaurante.enums.SituacaoComandaEnum;
import br.ucsal._20211.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal._20211.testequalidade.restaurante.exception.MesaOcupadaException;

public class Comanda {

	private static Integer seq = 0;

	private Integer codigo;

	private Mesa mesa;

	private Map<Item, Integer> itens = new HashMap<>();

	private SituacaoComandaEnum situacao = SituacaoComandaEnum.ABERTA;

	public Comanda(Mesa mesa) throws MesaOcupadaException {
		super();
		codigo = ++seq;
		this.mesa = mesa;
	}

	public void incluirItem(Item item, Integer qtd) throws ComandaFechadaException {
		Integer qtdAtual = 0;
		if (itens.containsKey(item)) {
			qtdAtual = itens.get(item);
		}
		qtdAtual += qtd;
		itens.put(item, qtdAtual);
	}

	public void setSituacao(SituacaoComandaEnum situacao) {
		this.situacao = situacao;
	}

	public SituacaoComandaEnum getSituacao() {
		return situacao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public Map<Item, Integer> getItens() {
		return new HashMap<>(itens);
	}

}
