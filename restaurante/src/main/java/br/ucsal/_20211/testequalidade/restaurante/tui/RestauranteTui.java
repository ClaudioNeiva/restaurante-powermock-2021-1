package br.ucsal._20211.testequalidade.restaurante.tui;

import java.util.Scanner;

import br.ucsal._20211.testequalidade.restaurante.business.RestauranteBO;
import br.ucsal._20211.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal._20211.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal._20211.testequalidade.restaurante.exception.RegistroNaoEncontrado;

public class RestauranteTui {

	private static final String VALOR_COMANDA = "Valor comanda = %10.2f";
	private static final String INFORME_QUANTIDADE_ITEM = "Informe a quantidade do item:";
	private static final String INFORME_CODIGO_ITEM = "Informe o código da item:";
	private static final String MES_INFORME_CODIGO_COMANDA = "Informe o código da comanda:";
	private static final String MENS_INFORME_NUMERO_MESA = "Informe o número da mesa:";

	private static Scanner sc = new Scanner(System.in);

	private RestauranteTui() {
	}

	public static void abrirComanda() {
		Integer numeroMesa = obterInteiro(MENS_INFORME_NUMERO_MESA);
		try {
			System.out.println(RestauranteBO.abrirComanda(numeroMesa));
		} catch (RegistroNaoEncontrado | MesaOcupadaException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void incluirItemComanda() {
		Integer codigoComanda = obterInteiro(MES_INFORME_CODIGO_COMANDA);
		Integer codigoItem = obterInteiro(INFORME_CODIGO_ITEM);
		Integer qtdItem = obterInteiro(INFORME_QUANTIDADE_ITEM);
		try {
			RestauranteBO.incluirItemComanda(codigoComanda, codigoItem, qtdItem);
		} catch (RegistroNaoEncontrado | ComandaFechadaException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void fecharComanda() {
		Integer codigoComanda = obterInteiro(MES_INFORME_CODIGO_COMANDA);
		try {
			Double valorComanda = RestauranteBO.fecharComanda(codigoComanda);
			System.out.println(String.format(VALOR_COMANDA, valorComanda));
		} catch (RegistroNaoEncontrado | ComandaFechadaException e) {
			System.out.println(e.getMessage());
		}
	}

	private static Integer obterInteiro(String mesagem) {
		System.out.println(mesagem);
		Integer valor = sc.nextInt();
		sc.nextLine();
		return valor;
	}

}
