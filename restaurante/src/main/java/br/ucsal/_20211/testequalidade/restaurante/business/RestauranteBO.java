package br.ucsal._20211.testequalidade.restaurante.business;

import br.ucsal._20211.testequalidade.restaurante.domain.Comanda;
import br.ucsal._20211.testequalidade.restaurante.domain.Item;
import br.ucsal._20211.testequalidade.restaurante.domain.Mesa;
import br.ucsal._20211.testequalidade.restaurante.enums.SituacaoComandaEnum;
import br.ucsal._20211.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal._20211.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal._20211.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal._20211.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal._20211.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal._20211.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal._20211.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBO {

	private RestauranteBO() {
	}

	public static Integer abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException {
		Mesa mesa = MesaDao.obterPorNumero(numeroMesa);
		if (SituacaoMesaEnum.LIVRE.equals(mesa.getSituacao())) {
			Comanda comanda = new Comanda(mesa);
			ComandaDao.incluir(comanda);
			return comanda.getCodigo();
		}
		throw new MesaOcupadaException(numeroMesa);
	}

	public static void incluirItemComanda(Integer codigoComanda, Integer codigoItem, Integer qtdItem)
			throws RegistroNaoEncontrado, ComandaFechadaException {
		Comanda comanda = ComandaDao.obterPorCodigo(codigoComanda);
		Item item = ItemDao.obterPorCodigo(codigoItem);
		comanda.incluirItem(item, qtdItem);
	}

	public static Double fecharComanda(Integer codigoComanda) throws RegistroNaoEncontrado, ComandaFechadaException {
		Comanda comanda = ComandaDao.obterPorCodigo(codigoComanda);
		if (SituacaoComandaEnum.ABERTA.equals(comanda.getSituacao())) {
			Double total = 0d;
			for (Item item : comanda.getItens().keySet()) {
				total += item.getValorUnitario() * comanda.getItens().get(item);
			}
			comanda.setSituacao(SituacaoComandaEnum.FECHADA);
			return total;
		}
		throw new ComandaFechadaException(codigoComanda);
	}

}
